package m1s1cprog;

public class ChaineCryptee {
	private String clair;
	private int decalage;
	
	public ChaineCryptee(String clair, int decalage) {
		this.clair = clair;
		this.decalage = decalage;
	}
	
	/**
	 * Renvoie la chaine en clair
	 *
	 * @return la chaine en clair
	 */
	public String decrypte() {
	    return clair;
	}
	
	/**
	 * Renvoie la chaine crypt�
	 * Si la chaine vaut null, le message "Chaine vaut Null" est envoy� a la place
	 *
	 * @return la chaine crypt�
	 */
	public String crypte() {
		if(clair == null)
			return "Chaine vaut Null";
		StringBuffer rep = new StringBuffer(clair);
		for(int i = 0; i < rep.length(); i++) {
			rep.replace(i, i+1, Character.toString(decaleCaractere(rep.charAt(i), decalage)));
		}
	    return rep.toString();
	}
	
	/**
	 * D�cale un caract�re majuscule.
	 * Les lettres en majuscule ('A' - 'Z') sont d�cal�es de <code>decalage</code>
	 * caract�res de fa�on circulaire. Les autres caract�res ne sont pas modifi�s.
	 *
	 * @param c le caract�re � d�caler
	 * @param decalage le d�calage � appliquer
	 * @return le caract�re d�cal�
	 */
	private static char decaleCaractere(char c, int decalage) {
	    return (c < 'A' || c > 'Z')? c : (char)(((c - 'A' + decalage) % 26) + 'A');
	}
}
