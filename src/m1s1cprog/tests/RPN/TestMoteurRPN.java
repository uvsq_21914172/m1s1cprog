package m1s1cprog.tests.RPN;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import m1s1cprog.RPN.MoteurRPN;
import m1s1cprog.RPN.Operation;
import m1s1cprog.RPN.errors.RPNAlgebreException;
import m1s1cprog.RPN.errors.RPNEmptyException;
import m1s1cprog.RPN.errors.RPNException;
import m1s1cprog.RPN.errors.RPNValueException;

class TestMoteurRPN {
	private MoteurRPN m;
	private ArrayList<Double> a;

	@BeforeEach
	void setUp() throws Exception {
		m = new MoteurRPN();
		a = new ArrayList<Double>();
	}
	
	@Test
	void ajouter() throws RPNException {
		a.add(1.0);
		m.ajouter(1.0);
		assertEquals(a, m.etatPile());
	}

	@Test
	void addition() throws RPNException {
		a.add(10.0);
		m.ajouter(3.0);
		m.ajouter(7.0);
		m.appliquer(Operation.PLUS);
		assertEquals(a, m.etatPile());
	}
	
	@Test
	void soustraction() throws RPNException {
		a.add(10.0);
		m.ajouter(20.0);
		m.ajouter(10.0);
		m.appliquer(Operation.MOINS);
		assertEquals(a, m.etatPile());
	}
	
	@Test
	void multiplication() throws RPNException {
		a.add(10.0);
		m.ajouter(2.0);
		m.ajouter(5.0);
		m.appliquer(Operation.MULT);
		assertEquals(a, m.etatPile());
	}
	
	@Test
	void division() throws RPNException {
		a.add(0.5);
		m.ajouter(1.0);
		m.ajouter(2.0);
		m.appliquer(Operation.DIV);
		assertEquals(a, m.etatPile());
	}
	
	@Test
	void divisionParZero() throws RPNValueException {
		m.ajouter(1.0);
		m.ajouter(0.0);
		Assertions.assertThrows(RPNAlgebreException.class, new Executable() {
			@Override
			public void execute() throws Throwable {
				// TODO Auto-generated method stub
				m.appliquer(Operation.DIV);
				}
		});
	}
	
	@Test
	void valeurHorsInterval() {
		Assertions.assertThrows(RPNValueException.class, new Executable() {
			@Override
			public void execute() throws Throwable {
				// TODO Auto-generated method stub
				m.ajouter(200);
				}
		});
	}
	
	@Test
	void retourSurPileVide() {
		Assertions.assertThrows(RPNEmptyException.class, new Executable() {
			@Override
			public void execute() throws Throwable {
				// TODO Auto-generated method stub
				m.etatPile();
				}
		});
	}
	
}
