package m1s1cprog.RPN;

import java.util.ArrayList;
import java.util.Scanner;

import m1s1cprog.RPN.errors.RPNEmptyException;
import m1s1cprog.RPN.errors.RPNException;
import m1s1cprog.RPN.errors.RPNSaisieException;
import m1s1cprog.RPN.errors.RPNValueException;

public class SaisieRPN {

	MoteurRPN moteur;
	Scanner sc;
	boolean state;
	
	public SaisieRPN() {
		moteur = new MoteurRPN();
		sc = new Scanner(System.in);
		state = true;
		System.out.println("Initialisation de la CalculatriceRPN");
	}
	
	public void attendreSaisie() throws RPNException {
		Operation op;
		String entree = sc.nextLine();
		if(entree.equalsIgnoreCase("exit"))
			state = false;
		else if((op = Operation.opFromSymbole(entree)) != Operation.NONE)
			try {
				moteur.appliquer(op);
			}
			catch(RPNException e)
			{
				throw e;
			}
		else {
			try {
				moteur.ajouter(Double.parseDouble(entree));
			}catch(NumberFormatException e){
				throw new RPNSaisieException("Saisie invalide");
			}catch(RPNValueException e)
			{
				throw e;
			}
		}
		try{
			afficherEtat();
		}catch(RPNEmptyException e) {
			throw e;
		}
	}
	
	public boolean etat() {
		return state;
	}
	
	public void afficherEtat() throws RPNEmptyException {
		ArrayList<Double> pile;
		try {
			pile = moteur.etatPile();
		}catch(RPNEmptyException e) {
			throw e;
		}
		System.out.println("Etat de la pile rpn:\n");
		for(double operande: pile) {
			System.out.print(operande + "  ");
		}
		System.out.print("\n");
	}
}
