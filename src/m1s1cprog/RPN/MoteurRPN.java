package m1s1cprog.RPN;

import java.util.ArrayList;
import java.util.Stack;

import m1s1cprog.RPN.errors.RPNAlgebreException;
import m1s1cprog.RPN.errors.RPNEmptyException;
import m1s1cprog.RPN.errors.RPNException;
import m1s1cprog.RPN.errors.RPNValueException;

public class MoteurRPN {
	private final static double min = -100;
	private final static double max = 100;
	private Stack<Double> operandes;
	
	
	 /**
	  * Constructeur par defaut
	  */
	public MoteurRPN() {
		operandes = new Stack<Double>();
	}
	
	/**
	 * Methode permettant d'ajoute un nombre a la pile d'operandes
	 * @throws RPNValueException 
	 * @params operande a ajouter
	 */
	public void ajouter(double operande) throws RPNValueException {
		if(operande > max)
			throw new RPNValueException("Operande trop grande : >" +max);
		if(operande > max)
			throw new RPNValueException("Operande trop petite : <" +min);
		operandes.push(operande);
	}
	
	/**
	 * Methode permettant d'appliquer une op�ration au deux op�randes
	 * en haut de la pile
	 * @params operation a appliquer
	 */
	public void appliquer(Operation operation) throws RPNException {
		double op1, op2;
		if(operandes.empty())
			throw new RPNEmptyException("La pile RPN est vide.");
		op1 = operandes.pop();
		if(operandes.empty()){
			operandes.push(op1);
			throw new RPNEmptyException("Le nombre d'operandes est insufisant.");
			}
		op2 = operandes.pop();
		try {
			operandes.push(operation.eval(op1,op2));
		}catch(RPNAlgebreException e) {
			operandes.push(op2);
			operandes.push(op1);
			throw e;
		}
	}
	
	/**
	 * Methode renvoyant une Liste d�crivant l'etat de la pile
	 */
	public ArrayList<Double> etatPile() throws RPNEmptyException{
		if(operandes.empty())
			throw new RPNEmptyException("La pile RPN est vide.");
		return new ArrayList<Double>(operandes);
	}
}
