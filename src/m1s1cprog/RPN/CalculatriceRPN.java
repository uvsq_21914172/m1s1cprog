package m1s1cprog.RPN;

import m1s1cprog.RPN.errors.RPNException;

public class CalculatriceRPN {

	public static void main(String[] args) {
		SaisieRPN saisie = new SaisieRPN();
		
		while(saisie.etat()) {
			System.out.println("Saisie en attente:");
			try {
				saisie.attendreSaisie();
			} catch (RPNException e) {
				System.out.println(e.getMessage());
			}
		}
	}

}
