package m1s1cprog.RPN;

import m1s1cprog.RPN.errors.RPNAlgebreException;

public enum Operation {
	
	PLUS("+"){
		@Override
		double eval(double op1, double op2) {
			return op1 + op2;
		}
	},
	MOINS("-"){
		@Override
		double eval(double op1, double op2) {
			return op2 - op1;
		}
	},
	MULT("*"){
		@Override
		double eval(double op1, double op2) {
			return op1 * op2;
		}
	},
	DIV("/"){
		@Override
		double eval(double op1, double op2) throws RPNAlgebreException {
			if(op1 == 0)
				throw new RPNAlgebreException("Tentative de division par zero!");
			return op2 / op1;
		}
	},
	NONE{
		@Override
		double eval(double op1, double op2){
			return 0;
		}
	};

	private String symbole;
	
	/**
	 * Constructeur de l'operation
	 * @params symbole
	 */
	
	private Operation(String symbole) {
		this.symbole = symbole;
	}
	
	/**
	 * Constructeur de l'operation
	 * @params symbole
	 */
	
	private Operation() {
	}
	
	/**
	 * Operation correspondant a l'enum
	 * @params Operande en haut de pile
	 * @params Second operande
	 * @return le resultat de l'operation
	 */
	
	abstract double eval(double op1, double op2) throws RPNAlgebreException;
	/**
	 * Fonction renvoyant l'enum correspondant au symbole
	 * @param symbole
	 * @return l'operation correspondante ou NONE
	 */
	public static Operation opFromSymbole(String symbole) {
		if(symbole.equals(PLUS.symbole))
			return PLUS;
		else if(symbole.equals(MOINS.symbole))
			return MOINS;
		else if(symbole.equals(MULT.symbole))
			return MULT;
		else if(symbole.equals(DIV.symbole))
			return DIV;
		else
			return NONE;
	}
}
