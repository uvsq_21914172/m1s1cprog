package m1s1cprog.RPN.errors;

public class RPNEmptyException  extends RPNMoteurException{

	public RPNEmptyException(String message) {
		super(message);
	}
}
