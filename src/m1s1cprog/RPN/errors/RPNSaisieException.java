package m1s1cprog.RPN.errors;

public class RPNSaisieException extends RPNException{

	public RPNSaisieException(String message) {
		super(message);
	}
}
