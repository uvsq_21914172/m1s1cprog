package m1s1cprog.RPN.errors;

public class RPNValueException extends RPNMoteurException{
	
	public RPNValueException(String message) {
		
		super(message);
	}
}
