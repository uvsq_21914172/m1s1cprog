package m1s1cprog.RPN.errors;

public class RPNAlgebreException extends RPNMoteurException{

	public RPNAlgebreException(String message) {
		super(message);
	}
}
