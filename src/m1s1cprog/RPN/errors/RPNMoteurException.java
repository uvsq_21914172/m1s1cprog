package m1s1cprog.RPN.errors;

public class RPNMoteurException extends RPNException{

	public RPNMoteurException(String message) {
		super(message);
	}
}
