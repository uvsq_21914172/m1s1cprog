package m1s1cprog.RPN.errors;

public class RPNException  extends Exception{

	public RPNException(String message) {
		super(message);
	}
}
